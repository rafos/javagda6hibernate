import dao.*;
import domain.many2many.Activity;
import domain.many2many.Season;
import domain.many2many.Sportsman;
import domain.one2one.Address;
import domain.one2one.Customer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

import java.util.List;
import java.util.Optional;

public class Application {

	public static void main(String[] args) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();



			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
			sessionFactory.close();
		}
	}
}
