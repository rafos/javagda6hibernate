package dao;

import domain.many2many.Activity;
import domain.many2many.Sportsman;
import org.hibernate.Session;

import java.util.List;

public class SportsmanDAO {

	private final Session session;

	public SportsmanDAO(Session session) {
		this.session = session;
	}

	// CRUD:

	public List<Sportsman> getAll() {
		return session.createQuery("from Sportsman").getResultList();
	}

	public List<Sportsman> getAllByCountry(String country) {
		return session
				.createQuery("from Sportsman s where s.country = :country")
				.setParameter("country", country)
				.getResultList();
	}

	public List<Sportsman> getAllByActivity(int activityId) {
		return session
				.createQuery("select s from Sportsman s join s.activities a where a.id = :activityId")
				.setParameter("activityId", activityId)
				.getResultList();
	}

	public Sportsman addActivity(int sportsmanId, int activityId) {
		Sportsman sportsman = session.get(Sportsman.class, sportsmanId);
		Activity activity = session.get(Activity.class, activityId);
		sportsman.addActivity(activity);
		return sportsman;
	}

	// TODO:
	public Sportsman removeActivity(int sportsmanId, int activityId) {
		return null;
	}
}
