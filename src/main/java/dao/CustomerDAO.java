package dao;

import domain.one2one.Address;
import domain.one2one.Customer;
import org.hibernate.Session;

import java.util.List;
import java.util.Optional;

public class CustomerDAO {

	private final Session session;

	public CustomerDAO(Session session) {
		this.session = session;
	}

	public Customer create(String customerName, int addressId) {
		Customer customer = new Customer();
		customer.setName(customerName);

		Address address = session.find(Address.class, addressId);
		customer.setAddress(address);

		if (address != null) {
			address.setCustomer(customer);
		}

		session.persist(customer);

		return customer;
	}

	public Customer get(int id) {
		return session.get(Customer.class, id);
	}

	public Optional<Customer> find(int id) {
		return session.byId(Customer.class).loadOptional(id);
	}

	public List<Customer> getAll() {
		return session.createQuery("from Customer").getResultList();
	}

	public Customer update(int id, String name) {
		Customer customer = session.get(Customer.class, id);
		customer.setName(name);
		return customer;
	}

	public Customer changeAddress(int customerId, int addressId) {
		Customer customer = session.get(Customer.class, customerId);
		Address address = session.get(Address.class, addressId);
		customer.setAddress(address);
		address.setCustomer(customer);
		return customer;
	}

	public Customer unsetAddress(int customerId) {
		Customer customer = session.get(Customer.class, customerId);
		Address address = customer.getAddress();
		if (address != null) {
			address.setCustomer(null);
		}
		customer.setAddress(null);
		return customer;
	}

	public Customer remove(int id) {
		Customer customer = session.get(Customer.class, id);
		Address address = customer.getAddress();
		if (address != null) {
			address.setCustomer(null);
		}
		session.remove(customer);
		return customer;
	}

	public Customer removeWithAddress(int id) {
		Customer customer = session.get(Customer.class, id);
		Address address = customer.getAddress();
		if (address != null) {
			address.setCustomer(null);
			session.remove(address);
		}
		session.remove(customer);
		return customer;
	}
}
