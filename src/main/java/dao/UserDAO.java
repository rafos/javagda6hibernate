package dao;

import domain.one2many.Phone;
import domain.one2many.Users;
import org.hibernate.Session;

import java.util.List;
import java.util.Optional;

public class UserDAO {

	private final Session session;

	public UserDAO(Session session) {
		this.session = session;
	}

	// TODO:
	public Users create(String name) {
		Users users = new Users();
		users.setName(name);
		session.persist(users);
		return users;
	}

	// TODO:
	public Users get(int id) {
		return null;
	}

	// TODO:
	public Optional<Users> find(int id) {
		return null;
	}

	// TODO:
	public List<Users> getAll() {
		return null;
	}

	// TODO:
	public Users update(int id, String name) {
		return null;
	}

	// TODO:
	public Users addPhone(int userId, int phoneId) {
		Users users = session.get(Users.class, userId);
		Phone phone = session.get(Phone.class, phoneId);

		users.addPhone(phone);
		return users;
	}

	// TODO:
	public Users removePhone(int userId, int phoneId) {
		return null;
	}

	public Users remove(int id) {
		Users users = session.get(Users.class, id);
		users.setPhones(null);
		session.remove(users);
		return users;
	}

	public Users removeWithPhones(int id) {
		Users users = session.get(Users.class, id);
		for (Phone phone : users.getPhones()) {
			session.remove(phone);
		}
		users.setPhones(null);
		session.remove(users);
		return users;
	}
}
