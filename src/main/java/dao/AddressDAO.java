package dao;

import domain.one2one.Address;
import domain.one2one.Customer;
import org.hibernate.Session;

import java.util.List;
import java.util.Optional;

public class AddressDAO {

	private static final Class<Address> ADDRESS = Address.class;

	private final Session session;

	public AddressDAO(Session session) {
		this.session = session;
	}

	public Address create(String street, String town) {
		Address address = new Address();
		address.setStreet(street);
		address.setTown(town);
		session.persist(address);
		return address;
	}

	public Address get(int id) {
		return session.get(ADDRESS, id);
	}

	public Optional<Address> find(int id) {
		return session.byId(ADDRESS).loadOptional(id);
	}

	public List<Address> getAll() {
		return session.createQuery("from Address").getResultList();
	}

	public List<Address> getAllByTown(String town) {
		return session
				.createQuery("from Address a where a.town = :town")
				.setParameter("town", town)
				.getResultList();
	}

	public Address update(int id, String street, String town) {
		Address address = session.get(ADDRESS, id);
		address.setStreet(street);
		address.setTown(town);
		return address;
	}

	public Address remove(int id) {
		Address address = session.get(ADDRESS, id);
		Customer customer = address.getCustomer();
		if (customer != null) {
			customer.setAddress(null);
		}
		session.remove(address);
		return address;
	}
}
