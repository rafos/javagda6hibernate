package dao;

import domain.one2many.Book;
import domain.one2many.Library;
import org.hibernate.Session;

import java.util.Optional;

public class LibraryDAO {

	private final Session session;

	public LibraryDAO(Session session) {
		this.session = session;
	}

	public Library create(String name, String town) {
		Library library = new Library();
		library.setName(name);
		library.setTown(town);
		session.persist(library);
		return library;
	}

	public Library get(int id) {
		//  TODO: zamienic na literal klasy
		return session.get(Library.class, id);
	}

	public Optional<Library> find(int id) {
		Library library = session.get(Library.class, id);
		return Optional.ofNullable(library);
	}

	public Library update(int id, String name, String town) {
		Library library = session.get(Library.class, id);
		library.setName(name);
		library.setTown(town);
		return library;
	}

	public Library addBookToLibrary(int libraryId, int bookId) {
		Library library = session.get(Library.class, libraryId);
		Book book = session.get(Book.class, bookId);
		library.addBook(book);
		return library;
	}

	public Library removeBookFromLibrary(int libraryId, int bookId) {
		Library library = session.get(Library.class, libraryId);
		Book book = session.get(Book.class, bookId);
		library.removeBook(book);
		return library;
	}

	// optional?
	public Library remove(int id) {
		Library library = session.get(Library.class, id);
		for (Book book : library.getBooks()) {
			book.setLibrary(null);
		}
		library.setBooks(null);
		session.remove(library);
		return library;
	}
}

