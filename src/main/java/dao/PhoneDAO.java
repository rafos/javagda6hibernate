package dao;

import domain.one2many.Phone;
import org.hibernate.Session;

import java.util.List;
import java.util.Optional;

public class PhoneDAO {

	private final Session session;

	public PhoneDAO(Session session) {
		this.session = session;
	}

	public Phone create(String number) {
		Phone phone = new Phone();
		phone.setNumber(number);
		session.persist(phone);
		return phone;
	}

	// TODO:
	public Phone get(int id) {
		return null;
	}

	// TODO:
	public Optional<Phone> find(int id) {
		return null;
	}

	// TODO:
	public List<Phone> getUserPhones(int userId) {
		return null;
	}

	// TODO:
	public Phone update(int id, String number) {
		return null;
	}

	public Phone remove(int id) {
		Phone phone = session.get(Phone.class, id);
		session.remove(phone);
		return phone;
	}
}
