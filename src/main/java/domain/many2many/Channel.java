package domain.many2many;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

public class Channel {

	private int id;

	private String name;

	private String subject;

	@ManyToMany
	@JoinTable(name = "CHANNEL_TO_MEMBER",
			joinColumns = {@JoinColumn(name = "CHANNEL_ID")},
			inverseJoinColumns = {@JoinColumn(name = "MEMBER_ID")})
	private List<Member> members = new ArrayList<>();
}
