package domain.many2many;

public enum Season {
	SPRING,
	SUMMER,
	AUTUMN,
	WINTER;
}
