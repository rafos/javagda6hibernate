package domain.many2many;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Sportsman {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String name;

	private String country;

	@ManyToMany
	@JoinTable(name = "SPORTSMAN_TO_ACTIVITY",
		joinColumns = {@JoinColumn(name = "SPORTSMAN_ID")},
		inverseJoinColumns = {@JoinColumn(name = "ACTIVITY_ID")})
	private List<Activity> activities = new ArrayList<>();


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<Activity> getActivities() {
		return activities;
	}

	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}

	public void addActivity(Activity activity) {
		this.activities.add(activity);
	}

	public void removeActivity(Activity activity) {
		this.activities.remove(activity);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Sportsman sportsman = (Sportsman) o;
		return id == sportsman.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return "Sportsman{" +
				"id=" + id +
				", name='" + name + '\'' +
				", country='" + country + '\'' +
				", activities=" + activities +
				'}';
	}
}
