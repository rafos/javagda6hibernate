package domain.one2one;

import javax.persistence.*;

@Entity
public class Player {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String nickname;

	@OneToOne
	@PrimaryKeyJoinColumn
	private Avatar avatar;


}
