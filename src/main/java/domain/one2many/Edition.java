package domain.one2many;

public enum Edition {
	DVD,
	BLURAY,
	VIDEOCD;
}
