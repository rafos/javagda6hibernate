package domain.one2many;

import javax.persistence.*;
import java.util.List;

@Entity
public class Users {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String name;

	@OneToMany
	@JoinColumn(name = "USER_ID")
	private List<Phone> phones;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Phone> getPhones() {
		return phones;
	}

	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}

	public void addPhone(Phone phone) {
		this.phones.add(phone);
	}

	public void removePhone(Phone phone) {
		this.phones.remove(phone);
	}

	@Override
	public String toString() {
		return "Users{" +
				"id=" + id +
				", name='" + name + '\'' +
				", phones=" + phones +
				'}';
	}
}
