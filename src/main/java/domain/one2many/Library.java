package domain.one2many;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Library {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String name;

	private String town;

	@OneToMany(mappedBy = "library")
	private List<Book> books = new ArrayList<>();


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public void addBook(Book book) {
		getBooks().add(book);
		book.setLibrary(this);
	}

	public void removeBook(Book book) {
		getBooks().remove(book);
		book.setLibrary(null);
	}

	@Override
	public String toString() {
		return "Library{" +
				"id=" + id +
				", name='" + name + '\'' +
				", town='" + town + '\'' +
				'}';
	}
}
